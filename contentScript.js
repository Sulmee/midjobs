console.log("Content script");

chrome.storage.sync.get('enabled', function (data) {
    const bool = JSON.parse(data.enabled);
    if (bool) {
        var earnMoney = document.getElementById('ctl00_SidebarMenu1_EarnMenu');

        var welcomeModal = document.getElementById("welcomeModal");
        if (welcomeModal) {
            document.querySelector("#welcomeModal > div > div > div.modal-header > button").click()
        }
        var availableVideos = earnMoney ? earnMoney.innerText.split('\n')[1] : undefined;
        if (availableVideos && availableVideos > 0) {

            var availableVideosLink = earnMoney.firstElementChild;
            availableVideosLink.click();

            var youtubeLink = document.getElementById("ctl00_SidebarMenu1_Earn18").firstElementChild;
            youtubeLink.click();
        }
   }
});

