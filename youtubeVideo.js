chrome.storage.sync.get(['enabled', 'viewed'], function (data) {
    const bool = data.enabled;
    if (bool) {
        if (window.location.search === "" || window.location.search === undefined) {
            var videosHTMLCollection = document.getElementsByClassName(
                'col-lg-3 video-main'
            );

            var videos = Object.entries(videosHTMLCollection).map(
                (x) => x[1].firstElementChild
            );
            var viewed = data.viewed;
          
            var hrefs = videos.map(x=> x.href)
            var diff = _.difference(hrefs, data.viewed)[0];
            var foundVideo = _.find(videos, { href: diff} )
            
            if (foundVideo) {
                chrome.storage.sync.set({ viewed: [...viewed, diff] })
                foundVideo.click()
            }
        } else {
            window.setTimeout(function () {
                runScript();
                window.setTimeout(function () {

                    var earnMoney = document.getElementById('ctl00_SidebarMenu1_EarnMenu');
                    var availableVideos = earnMoney ? earnMoney.innerText.split('\n')[1] : undefined;
                    if (availableVideos && availableVideos > 0) {
                        var youtubeLink = document.getElementById("ctl00_SidebarMenu1_Earn18").firstElementChild;
                        youtubeLink.click();
                    }
                }, Math.floor(Math.random() * 5000) + 35000);
            }, Math.floor(Math.random() * 5000) + 10000)
        }
          

        function runScript() {
            var script = document.createElement('script');
            script.id = 'tmpScript';
            script.type = 'text/javascript';
            script.text = 'player.playVideo()';
            (document.body || document.head || document.documentElement).appendChild(script);
        }
    }
});
