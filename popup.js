// Copyright 2018 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

'use strict';

let changeColor = document.getElementById('changeColor');

chrome.storage.sync.get('enabled', function(data) {
  const bool = JSON.parse(data.enabled);
  changeColor.innerText = bool ? 'enabled': 'disabled';
});

changeColor.onclick = function(element) {
  let status = element.target.innerText;
  const statusMap = {
    enabled: true,
    disabled: false
  };

  chrome.storage.sync.set({ 'enabled': !statusMap[status]});
  document.getElementById('changeColor').innerHTML = !statusMap[status] ?  'enabled' : 'disabled';
};

const clearCache = document.getElementById('clearCache');
clearCache.onclick = function(element) {
  chrome.storage.sync.set({ viewed: []}, function() {
    alert('cache cleared')
  });
}