// Copyright 2018 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

'use strict';

chrome.runtime.onInstalled.addListener(function () {
    chrome.declarativeContent.onPageChanged.removeRules(undefined, function () {
        chrome.declarativeContent.onPageChanged.addRules([{
            conditions: [new chrome.declarativeContent.PageStateMatcher({
                pageUrl: {urlMatches: 'midjobs.com'},
            })],
            actions: [new chrome.declarativeContent.ShowPageAction()]
        }]);
    });
    chrome.storage.sync.set({ enabled: false});
    chrome.storage.sync.set({ viewed: []});
    chrome.storage.sync.set({ viewed2: 'blah'});

});
// chrome.runtime.onMessage.addListener(
//     function (message, callback) {
//         if (message == 'runContentScript') {
//             chrome.tabs.executeScript({
//                 file: 'contentScript.js'
//             });
//         }
// });
